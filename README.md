# laser-following-robot

## To run the code
**Use cmake . to compile the code
**use sudo ./laserFollower to run the program
**Press escape to close the program (if not working, click the camera window then press escape)
## Basic Git commands
**Clone repo**: git clone https://gitlab.com/getchella/laser-following-robot.git<br/>
**Create new branch**: git checkout -b name-of-branch<br/>
**Open existing branch**: git checkout name-of-branch<br/>
**Add new file in commit**: git add filename.txt<br/>
**Commit changes**: git commit -a -m "Small description of changes"<br/>
**Push changes**: git push origin name-of-branch<br/>
**Remove commit before push**: git reset HEAD~1

