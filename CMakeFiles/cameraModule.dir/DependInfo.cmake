# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kaiser/Desktop/SeniorDesign/laser-following-robot/src/CameraModule.cpp" "/home/kaiser/Desktop/SeniorDesign/laser-following-robot/CMakeFiles/cameraModule.dir/src/CameraModule.cpp.o"
  "/home/kaiser/Desktop/SeniorDesign/laser-following-robot/src/fsm.cpp" "/home/kaiser/Desktop/SeniorDesign/laser-following-robot/CMakeFiles/cameraModule.dir/src/fsm.cpp.o"
  "/home/kaiser/Desktop/SeniorDesign/laser-following-robot/src/main.cpp" "/home/kaiser/Desktop/SeniorDesign/laser-following-robot/CMakeFiles/cameraModule.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/libcamera"
  "src"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
