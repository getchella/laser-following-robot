#include <lccv.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/types.hpp>
#include <cmath>
#include <utility>
#include <vector>

#include "CameraModule.h"

using namespace cv;

/* Returns the grid number specifying the location of the laser */
int gridLocation(Point &laserLocation, int stepWidth, int stepHeight, int gridCols, int gridRows)
{
    // 1 indexed
    return (round(laserLocation.x/stepWidth) + gridRows * round(laserLocation.y/stepHeight)) + 1;
}

void drawGrid(Mat &image, int numRows, int numCols, int stepWidth, int stepHeight)
{
    for(int i = 1; i < image.rows/stepHeight; i++)
    {
        // line(image, start, end, color, thickness)
        if(i == numRows-1)
        {
            // drawing bottom backwards movement indicator (red line)
            line(image, Point(0,i*stepHeight), Point(image.cols, i*stepHeight), Scalar(0,0,255), 2); // bgr color code
            continue;
        }
        line(image, Point(0,i*stepHeight), Point(image.cols, i*stepHeight), Scalar(0,255,255),2); // bgr color code
    }

    for(int i = 1; i < image.cols/stepWidth; i++)
    {
        if(i == round(numCols/2) || i == round(numCols/2) + 1)
        {
            // drawing center "green lane"
            line(image, Point(i*stepWidth,0), Point(i*stepWidth, image.rows), Scalar(0,255,0), 2);
            continue;

        }
        line(image, Point(i*stepWidth,0), Point(i*stepWidth, image.rows), Scalar(0,255,255),2);
    }

}

int drawCircles(Mat &image, Mat &thresh, std::vector<cv::Vec3f> &circles, int stepWidth, int stepHeight, int gridCols, int gridRows)
{
    cv::HoughCircles(thresh,circles,cv::HOUGH_GRADIENT, 1,50,20,10, 2,0);

    static cv::Point center;
	static int gridLoc;
    for(size_t i = 0; i < circles.size(); i++)
    {
        cv::Vec3i c = circles[i];
        center = cv::Point(c[0],c[1]);
        gridLoc = gridLocation(center, stepWidth, stepHeight, gridCols, gridRows);

        #if CAMERA_DEBUG
        //std::cout << "Found " << circles.size() << " Circles" << std::endl;  
	#endif
        #if CAMERA_STREAM      
        cv::circle(image, center, 1, Scalar(0,10,100), 3, LINE_AA);
        int radius = c[2];
        circle(image, center, radius+20, Scalar(255,0,255), 3, LINE_AA);
        #endif
    }

    circles.clear();
    return gridLoc;
}


int laserDetect(Mat &image, Mat &thresh, int gridRows, int gridCols, int stepWidth, int stepHeight, std::vector<cv::Vec3f> &circles, Mat (&grb)[3])
{
    cv::split(image,grb); // splitting to grab blue channel
    cv::GaussianBlur(grb[2], grb[2], Size(7,7),0); // blurring
    cv::threshold(grb[2], thresh, 245,255,cv::THRESH_BINARY); // threshold anything less than pixel value 245 to be black, higher to be white
    drawGrid(image, gridRows, gridCols, stepWidth, stepHeight); 
    int gridLoc = drawCircles(image, thresh, circles, stepWidth, stepHeight, gridCols, gridRows);
    #if CAMERA_STREAM
    cv::imshow("Stream", image);
    #endif
    return gridLoc;
}


