#ifndef __DEFINES_H__
#define __DEFINES_H__

// From CameraModule.h
#define CAMERA_STREAM 1 // 1 for output camera, 0 for no output
#define CAMERA_DEBUG 1 // 1 for output to terminal, 0 for no output

// From main.cpp
#define FR_AI1  18  
#define FR_AI2  23
#define FR_PWMA 24
#define FL_BI1  17
#define FL_BI2  27
#define FL_PWMB 22
#define BR_BI1  16
#define BR_BI2  20  
#define BR_PWMB 21
#define BL_AI1  26 
#define BL_AI2  19
#define BL_PWMA 13


#define STATE_DEBUG 1



#endif