#include <iostream>
#include <string>
#include <stdint.h>
#ifdef __RPI__
#include <pigpio.h>
#endif
#include "fsm.h"

#define FR_AI1  18  
#define FR_AI2  23
#define FR_PWMA 24
#define FL_BI1  17
#define FL_BI2  27
#define FL_PWMB 22
#define BR_BI1  16
#define BR_BI2  20  
#define BR_PWMB 21
#define BL_AI1  26 
#define BL_AI2  19
#define BL_PWMA 13

/* TODO: Assign GPIO pins
#define DISTANCE_3
#define DISTANCE_2
#define DISTANCE_1
#define DISTANCE_0 
*/

#ifdef __RPI__
void initializeGPIO();
#endif

int main(void) {

    State_t *statePtr;
    int nextStateIndex;
    MotorOutputControl outputControl;

    #ifdef __DEBUG__
    std::string nameMOC[] = {"BRAKE", "MAX_SPEED", "REDUCE_SPEED", "REVERSE_ONLY"};
    #endif

    #ifdef __RPI__
    initializeGPIO();
    #endif

    statePtr = STOP;

    for (;;) {
        // TODO: Add motor control

        // TODO: Add laser detection

        // TODO: Add distance checking

        #ifdef __DEBUG__
        int distanceMsg;
        std::cout << "Enter next state: ";
        std::cin >> nextStateIndex;
        std::cout << "Enter distance message: ";
        std::cin >> distanceMsg;
        outputControl = parseDistance(distanceMsg, nextStateIndex);
        std::cout << "The current state is " << statePtr->curState << std::endl
                  << "After distance sensor read the output control will be " << nameMOC[outputControl] << std::endl
                  << "The next state will be S" << nextStateIndex << std::endl << std::endl;
        #endif

        statePtr = statePtr->Next[nextStateIndex];      // transition to next state
    }

    return 0;
}

#ifdef __RPI__
void initializeGPIO() {

    if (gpioInitialise() < 0) {
        // TODO: Add error
    }

    gpioSetMode(FR_AI1, PI_OUTPUT);
    gpioSetMode(FR_AI2, PI_OUTPUT);
    gpioSetMode(FR_PWMA, PI_OUTPUT);
    gpioSetMode(FL_BI1, PI_OUTPUT);
    gpioSetMode(FL_BI2, PI_OUTPUT);
    gpioSetMode(FL_PWMB, PI_OUTPUT);
    gpioSetMode(BL_AI1, PI_OUTPUT);
    gpioSetMode(BL_AI2, PI_OUTPUT); 
    gpioSetMode(BL_PWMA, PI_OUTPUT);
    gpioSetMode(BR_BI1, PI_OUTPUT);
    gpioSetMode(BR_BI2, PI_OUTPUT);
    gpioSetMode(BR_PWMB, PI_OUTPUT);
}
#endif