
#include <iostream>
#include <string>
#include <stdint.h>
#include <pigpio.h>
#include "fsm.h"
#include "CameraModule.h"
#include "defines.h"


void initializeGPIO();

int main()
{

    /* ----------------------- Camera Initialization ------------------------------ */
    cv::Mat image;
    lccv::PiCamera cam;
    cam.options->video_width=720;
    cam.options->video_height=480;
    cam.options->framerate=60;
    cam.options->verbose=true;
    #if CAMERA_STREAM
        cv::namedWindow("Stream", cv::WINDOW_AUTOSIZE);
    #endif

    cam.startVideo();

    int gridRows = 5;
    int gridCols = 5;
    int stepHeight = cam.options->video_height/gridRows;
    int stepWidth  = cam.options->video_width/gridCols;

    cv::Mat grb[3]; 
    cv::Mat thresh;
    std::vector<cv::Vec3f> circles;

    /********* FSM initialization*********/
    State_t *statePtr;
    int nextStateIndex;
    MotorOutputControl outputControl;

    initializeGPIO();

    int ch=0;
    statePtr = STOP;
    while(ch!=27)
    {
        // TODO: Add motor control

        // laser detection
        if(!cam.getVideoFrame(image,1000))
        {
            #if CAMERA_DEBUG
            std::cout<<"Timeout error"<<std::endl;
            #endif
        }
        else
        {
            nextStateIndex = laserDetect(image, thresh, gridRows, gridCols, stepWidth, stepHeight, circles, grb);
            ch=cv::waitKey(5); // decrease to minimum of 1 to increase speed
        }

	#if STATE_DEBUG
        std::cout << "The current state is " << statePtr->curState << std::endl
                  << "The next state will be S" << nextStateIndex << std::endl << std::endl;
	#endif

        // TODO: Add distance GPIO read

        statePtr = statePtr->Next[nextStateIndex];      // transition to next state
    }
    cam.stopVideo();
    #if CAMERA_STREAM
    cv::destroyWindow("Stream");
    #endif

    return 0;
}

void initializeGPIO() {

    if (gpioInitialise() < 0) {
        // TODO: Add error
    }

    gpioSetMode(FR_AI1, PI_OUTPUT);
    gpioSetMode(FR_AI2, PI_OUTPUT);
    gpioSetMode(FR_PWMA, PI_OUTPUT);
    gpioSetMode(FL_BI1, PI_OUTPUT);
    gpioSetMode(FL_BI2, PI_OUTPUT);
    gpioSetMode(FL_PWMB, PI_OUTPUT);
    gpioSetMode(BL_AI1, PI_OUTPUT);
    gpioSetMode(BL_AI2, PI_OUTPUT); 
    gpioSetMode(BL_PWMA, PI_OUTPUT);
    gpioSetMode(BR_BI1, PI_OUTPUT);
    gpioSetMode(BR_BI2, PI_OUTPUT);
    gpioSetMode(BR_PWMB, PI_OUTPUT);
}
