#ifndef __CAMERAMODULE_H__
#define __CAMERAMODULE_H__


#include <lccv.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/types.hpp>
#include <cmath>
#include <utility>
#include <vector>

#include "defines.h"
using namespace cv;


/* Returns the grid number specifying the location of the laser */
int gridLocation(Point &laserLocation, int stepWidth, int stepHeight, int gridCols, int gridRows);

void drawGrid(Mat &image, int numRows, int numCols, int stepWidth, int stepHeight);

int drawCircles(Mat &image, Mat &thresh, std::vector<cv::Vec3f> &circles, int stepWidth, int stepHeight, int gridCols, int gridRows);

int laserDetect(Mat &image, Mat &thresh, int gridRows, int gridCols, int stepWidth, int stepHeight, std::vector<cv::Vec3f> &circles, Mat (&grb)[3]);


#endif