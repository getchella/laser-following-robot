#ifndef __FSM_H__
#define __FSM_H__

#include <stdint.h>
#include <string>

#define NUM_STATES 26
#define WAIT_TIME 1     // ms

#define NO_OBJECTS    0
#define MID_RANGE_L   1
#define MID_RANGE_C   2
#define MID_RANGE_R   3
#define MID_RANGE_LC  4
#define MID_RANGE_CR  5
#define MID_RANGE_LR  6
#define MID_RANGE_LCR 7
#define TOO_CLOSE_L   8
#define TOO_CLOSE_C   9
#define TOO_CLOSE_R   10
#define TOO_CLOSE_LC  11
#define TOO_CLOSE_CR  12
#define TOO_CLOSE_LR  13
#define TOO_CLOSE_LCR 14

#define STOP &FSM[0]
#define S1 &FSM[1]
#define S2 &FSM[2]
#define S3 &FSM[3]
#define S4 &FSM[4]
#define S5 &FSM[5]
#define S6 &FSM[6]
#define S7 &FSM[7]
#define S8 &FSM[8]
#define S9 &FSM[9]
#define S10 &FSM[10]
#define S11 &FSM[11]
#define S12 &FSM[12]
#define S13 &FSM[13]
#define S14 &FSM[14]
#define S15 &FSM[15]
#define S16 &FSM[16]
#define S17 &FSM[17]
#define S18 &FSM[18]
#define S19 &FSM[19]
#define S20 &FSM[20]
#define S21 &FSM[21]
#define S22 &FSM[22]
#define S23 &FSM[23]
#define S24 &FSM[24]
#define S25 &FSM[25]



// controls how fast motors will turn based on distance sensor reading
typedef enum {
    BRAKE=0,
    MAX_SPEED,
    REDUCE_SPEED,
    REVERSE_ONLY,
} MotorOutputControl;

// 
typedef enum {
    LEFT=0,
    CENTER,
    RIGHT,
} LaserSection;

struct State {
    char curState[5];
    int FLOut;
    int FROut;
    int BLOut;
    int BROut;
    int waitTime;
    const struct State *Next[NUM_STATES];
};

typedef const struct State State_t;

extern State_t FSM[NUM_STATES];

MotorOutputControl parseDistance(int, int);


#endif 